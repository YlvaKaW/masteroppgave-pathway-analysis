import cobra
import numpy as np
import pandas as pd

from collections import defaultdict
from ecmtool.network import extract_sbml_stoichiometry
from ecmtool.conversion_cone import get_conversion_cone
from ecmtool.helpers import unsplit_metabolites, print_ecms_direct
from ecmtool.network import add_reaction_tags
from efmtool import calculate_efms


def check_feasibility(model, pathway, lb=0, ub=1000):
    """Check feasibility of pathway (EFP, ECM or MP) by maximizing growth rate.
    Returns solver status and optimal value.
    """

    # Map metabolites to boundary reactions
    boundary = {r.reactants[0].id: r.id for r in model.boundary}
    boundary.update({r.reactants[0].id: r.id for r in model.reactions
                     if r.id.startswith('EX_')})
    boundary.update({k + '_rev': v for k, v in boundary.items()})

    # Check feasibility of pathway in model
    with model as m:
        for r in m.boundary:
            r.bounds = 0, 0

        for x in pathway:
            r = m.reactions.get_by_id(boundary[x])
            if x.endswith('_rev'):
                r.bounds = -ub, -lb
            else:
                r.bounds = lb, ub

        s = m.optimize()

    return s.status, s.objective_value


def enumerate_efms(model, jvm_options=[]):
    """Enumerates EFMs for model."""

    stoichiometry = cobra.util.array.create_stoichiometric_matrix(model)
    reversibilities = [int(r.reversibility) for r in model.reactions]
    reaction_names = [r.id for r in model.reactions]
    metabolite_names = [m.id for m in model.metabolites]
    efms = calculate_efms(stoichiometry, reversibilities, reaction_names,
                          metabolite_names, jvm_options=jvm_options)

    # Workaround for numpy/pandas bug
    # https://stackoverflow.com/questions/30283836
    efms = np.array(efms).byteswap().newbyteorder()

    # Create EFM data frame
    efm_df = pd.DataFrame(efms, index=reaction_names).T

    return efm_df


def enumerate_ecms(model_path, tags=set(), print_ecms=True):
    """Loads model with given ID and returns its ECMs."""

    network = extract_sbml_stoichiometry(model_path, add_objective=True,
                                         determine_inputs_outputs=True,
                                         skip_external_reactions=True)

    if tags:
        # Tag reactions
        tags = [i for i, r in enumerate(network.reactions) if r.id[2:] in tags]
        tag_ids = add_reaction_tags(network, tags)

    # Some steps of compression only work when cone is in one orthant, so we
    # need to split external metabolites with direction "both" into two
    # metabolites, one of which is output, and one is input
    network.split_in_out(only_rays=False)

    # It is generally a good idea to compress the network before computation
    network.compress(verbose=True, SCEI=True, cycle_removal=True,
                     remove_infeasible=True)

    stoichiometry = network.N

    conv_cone = get_conversion_cone(stoichiometry,
                                    network.external_metabolite_indices(),
                                    network.reversible_reaction_indices(),
                                    network.input_metabolite_indices(),
                                    network.output_metabolite_indices(),
                                    verbose=True)

    # Since we have split the "both" metabolites, we now need to unsplit them
    cone_transpose, ids = unsplit_metabolites(np.transpose(conv_cone), network)
    cone = np.transpose(cone_transpose)

    # We can remove all internal metabolites, since their values are zero in
    # the conversions (by definition of internal)
    internal_ids = []
    for metab in network.metabolites:
        if not metab.is_external:
            id_ind = [ind for ind, id in enumerate(ids) if id == metab.id]
            if len(id_ind):
                internal_ids.append(id_ind[0])

    ids = list(np.delete(ids, internal_ids))
    cone = np.delete(cone, internal_ids, axis=1)

    # If you wish, one can print the ECM results:
    if print_ecms:
        print_ecms_direct(np.transpose(cone), ids)

    ecm_df = pd.DataFrame(cone, columns=ids).astype(float)

    return ecm_df


def get_efm_dfs(efm_df, objective='objective', subset=[], tol=1e-6):
    """Accepts an EFM matrix from efmtool. Returns EFM matrices."""

    efm_dfs = {}

    df = efm_df

    if not subset:
        subset = [x for x in df.columns if x.startswith('EX_')]

    # Reverse reversed reactions
    i = df.columns.str.endswith('_rev')
    df.iloc[:, i] = -df.iloc[:, i]
    df.columns = df.columns.str.replace('_rev', '')

    # Apply tolerance and get full EFM matrix
    df = df[df.abs() > tol].fillna(0)
    df = df.iloc[list(df.any(1)), list(df.any(0))].reset_index(drop=True)
    efm_dfs['full'] = df

    # Get unique exchange patterns
    subset = sorted(set(subset + [objective]) & set(df.columns))
    objective = objective.replace('DM_', '')
    df = np.sign(df.loc[:, subset]).drop_duplicates()
    df.columns = [x.replace('EX_', '').replace('SK_', '').replace('DM_', '')
                  for x in df.columns]
    df = df.iloc[list(df.any(1)), list(df.any(0))].reset_index(drop=True)
    efm_dfs['unique'] = df

    # Get unique exchange patterns with growth
    objective = objective.replace('DM_', '')
    df = df[df[objective] > tol].drop(objective, 1)
    df = df.iloc[list(df.any(1)), list(df.any(0))].reset_index(drop=True)
    efm_dfs['growth'] = df

    return efm_dfs


def get_efp_dfs(efm_df, objective='objective', tol=1e-6):
    """Accepts matrix with EFM exchange patterns. Returns EFP matrices."""

    efp_dfs = {}

    # Get exchange patterns as metabolite sets
    fps = get_sets(efm_df)

    # Find indices of FPs that are elementary
    e = np.zeros(len(fps), bool)
    for i, fp1 in enumerate(fps):
        # Union of all FPs that are subsets of this FP
        u = set().union(*[fp2 for fp2 in fps if fp2 < fp1])
        # FP is elementary if it cannot be expressed as union of other FPs
        e[i] = u < fp1

    # Get unique EFPs (all EFPs are unique)
    df = efm_df[e]
    df = df.iloc[list(df.any(1)), list(df.any(0))].reset_index(drop=True)
    efp_dfs['full'] = efp_dfs['unique'] = df

    # Get unique EFPs with growth   
    df = df[df[objective] > tol].drop(objective, 1)
    df = df.iloc[list(df.any(1)), list(df.any(0))].reset_index(drop=True)
    efp_dfs['growth'] = df

    return efp_dfs


def get_ecm_dfs(ecm_df, objective='objective', tol=1e-6):
    """Accepts an ECM matrix from ecmtool. Returns ECM matrices."""

    ecm_dfs = {}

    df = ecm_df

    # Convert columns to metabolite IDs
    df.columns = [x.replace('M_', '').replace('virtual_tag_R_EX_', '')
                  for x in df.columns]

    # Save full ECM matrix
    df = df.iloc[list(df.any(1)), list(df.any(0))].reset_index(drop=True)
    ecm_dfs['full'] = df

    # Get unique exchange patterns of ECMs
    df = np.sign(df).drop_duplicates()
    df = df.iloc[list(df.any(1)), list(df.any(0))].reset_index(drop=True)
    ecm_dfs['unique'] = df

    # Get unique exchange patterns of ECMs with growth
    df = df[df[objective] > tol].drop(objective, 1)
    df = df.iloc[list(df.any(1)), list(df.any(0))].reset_index(drop=True)
    ecm_dfs['growth'] = df

    return ecm_dfs


def get_mp_df_and_mps(mps):
    """Accepts MPs from mptool. Returns MPs as a matrix and metabolite sets."""

    # Convert MPs to metabolite sets
    mps = list(frozenset(x[3:] for x in mp) for mp in mps)

    # Build MP matrix
    d = defaultdict(dict)
    for i, mp in enumerate(mps):
        for x in mp:
            if x.endswith('_rev'):
                d[i][x[:-4]] = -1
            else:
                d[i][x] = 1
    mp_df = pd.DataFrame(d).T.fillna(0).astype(int).reset_index(drop=True)

    return mp_df, mps


def get_sets(df, tol=1e-6):
    """Converts a pathway matrix to metabolite sets."""
    ps = []
    for i, row in df.iterrows():
        p = set()
        for j, x in enumerate(row):
            if x > tol:
                p.add(df.columns[j])
            elif x < -tol:
                p.add(df.columns[j] + '_rev')
        p = frozenset(p)
        ps.append(p)
    return ps


# def set_bins(data):
#     # Set bins and binrange appropriate to dataset
#     min, max = np.min(data), np.max(data)
#     bins = max - min + 1
#     binrange = min - 0.5, max + 0.5
#     return (bins, binrange)


# def get_histplot_data(model_id):
#     # Read stat_df
#     data = pd.read_csv('results/ecm_mp_data_' + model_id + '.csv', index_col=0)

#     # Extract relevant data for each histogram plot
#     pathway_length = data.length
#     pathways = data.pathway
#     sets_in_ecms = data[data.pathway == 'ecm'].sets.astype(int)
#     sets_in_mps = data[data.pathway == 'mp'].sets.astype(int)

#     # Gather data and respective bin parameters
#     histplot_data = {'length': [pathway_length, pathways, set_bins(pathway_length)[0], set_bins(pathway_length)[1]], 
#                      'sets_in_ecms': [sets_in_ecms, set_bins(sets_in_ecms)[0], set_bins(sets_in_ecms)[1]], 
#                      'sets_in_mps': [sets_in_mps, set_bins(sets_in_mps)[0], set_bins(sets_in_mps)[1]]}
#     return histplot_data
