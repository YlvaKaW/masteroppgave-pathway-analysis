# -*- coding: utf-8 -*-
"""
Script for enumeration of minimal pathways (MPs) of human gut and microbiota.
All MPs found are stored in a set, where each MP is stored as a frozenset of metabolites.
"""

import gurobipy as grb
import mptool as mpt

model_name = 'HS_BT_EC_FP_LL_LP_ST'
model_dir = 'models/human_gut'
METABOLITE_NAMES = True  # Save metabolite names to ID as dictionary

# Load model
model = grb.read('../{}/{}.lp'.format(model_dir, model_name))

# Define exchange reactions as subset for finding MPs
subset = [v.varName for v in model.getVars() if '_IEX_' in v.varName or
          ('_EX_' in v.varName and v.varName.endswith('_rev'))]

# Define boundaries obtained by fva:
boundaries = dict()
f = open("results/HS_BT_EC_FP_LL_LP_ST_fva.csv", newline='\n')
for line in f:
    (key, val) = line.split(",", 1)  # Split only by first occurrence of comma
    val = eval(val.strip('"()"\r\n'))  # Convert bounds from string to tuple
    boundaries[key] = val

# Enumerate minimal sets of exchange reactions
mps, mcs, complete = mpt.find_mps(model,
                                  subset=subset,
                                  method='iterative',
                                  graph=False,
                                  random=True,
                                  bounds=boundaries,
                                  tol=1e-09,
                                  inf=1000,
                                  threads=0,
                                  max_mps=1,
                                  verbose=False,
                                  export=True)
