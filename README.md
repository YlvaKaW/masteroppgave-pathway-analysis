# Unbiased Analysis of Metabolic Networks
Different methods of unbiased analysis (Elementary flux modes (EFMs), Elementary flux patterns (EFPs), Elementary Conversion modes (ECMs), Minimal pathways (MPs) and random flux sampling) were applied on metabolite exchanges of constraint-based models ranging in size and complexity to explore relations between the selected methods and the differences in scalability and interpretations. 

We used  `ecmtool` (Clement et al. 2021), `efmtool` (Terzer and Stelling, 2008), and `mptool` (Øyås & Stelling, 2020) for pathway enumeration, and the flux sampling implementation of `pta` (Gollub et al. 2021). 

All data analysis is documented in the jupyter notebooks in the top directory. Raw data, scripts and results from enumeration and sampling can be found in respective subdirectories.
# Installation
We followed the installation guides provided on respective gitpages. ([ecmtool](https://github.com/SystemsBioinformatics/ecmtool), [mptool](https://gitlab.com/csb.ethz/mptool), [pta](https://gitlab.com/csb.ethz/pta)). The gurobi optimizer was used as solver for `mptool` and `pta`. CPLEX was used for `ecmtool`.

The anaconda environment used for this project can be recreated by entering the command: `conda create --name ENV_NAME --file unbiased_analysis.yaml`.