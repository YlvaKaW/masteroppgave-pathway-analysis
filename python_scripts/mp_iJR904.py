# -*- coding: utf-8 -*-
"""
Script for enumeration of minimal pathways (MPs). All MPs found are stored in a set,
where each MP is stored as a frozenset.

A dictionary with key pair ID:metabolite name is created as well
"""

import cobra
import mptool as mpt

model_name = 'iJR904'

# Load model
model = cobra.io.read_sbml_model(f'genome_scale_networks/models/{model_name}_ecm.xml')

# Define growth requirements for model for analysis of flux bounds
model.reactions.BIOMASS_Ecoli.lower_bound = 0.1
model.reactions.EX_glc__D_e.upper_bound = -0.1
# Kreve oksygeninntak

# Create subset
subset = list(r.id for r in model.exchanges)

# Split reactions
irreversible = mpt.make_irreversible(model=model, subset=subset)
subset_irreversible = set(r.id for r in irreversible)

# Enumerate minimal sets of exchange reactions
mps, mcs, complete = mpt.find_mps(model,
                                  subset=subset_irreversible,
                                  method='iterative',
                                  graph=True,
                                  random=False,
                                  bounds=0,
                                  tol=1e-09,
                                  inf=1000,
                                  threads=0,
                                  max_mps=0,
                                  max_t=0,
                                  verbose=True,
                                  export=True)
