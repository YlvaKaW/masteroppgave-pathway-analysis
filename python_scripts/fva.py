"""
A program that runs mptool's flux variability analysis (fva)
so that bounds can be easily determined.
"""

import gurobipy
import mptool as mpt
import csv

model_name = 'HS_BT_EC_FP_LL_LP_ST'
model_dir = 'models/human_gut'

# Load gurobi model
grb_model = gurobipy.read('{}/{}.lp'.format(model_dir, model_name))

# Run fva to obtain reasonable flux bounds for the model
bounds = mpt.fva(grb_model, verbose=False)

# Write bounds to csv
out_dir = 'results'
file = open("{}/{}_fva.csv".format(out_dir, model_name), "w")
writer = csv.writer(file)
for key, value in bounds.items():
    writer.writerow([key, value])
file.close()
