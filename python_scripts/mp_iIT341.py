# -*- coding: utf-8 -*-
"""
Script for enumeration of minimal pathways (MPs). All MPs found are stored in a set,
where each MP is stored as a frozenset.

A dictionary with key pair ID:metabolite name is created as well. See documentation of `extract_ecm_ids` in `functions.py`.
"""
import cobra
import mptool as mpt
from functions import*

model_name = 'iIT341'
METABOLITE_NAMES = True  # Save metabolite names to ID as dictionary

# Load model
model = cobra.io.read_sbml_model(f'genome_scale_networks/models/{model_name}_ecm_minII.xml')

# Define growth requirements for model for analysis of flux bounds
model.reactions.BIOMASS_HP_published.lower_bound = 1e-4

# Define subset of reactions.
# Subset is equivalent to inputs and outputs defined in the respective ecm shellscript.
# (See processing_for_recreating_experiments.ipynb under 'genome_scale_networks')
subset = [r.id for r in model.boundary]

# Generate and write dictionary to file for plotting
if METABOLITE_NAMES:
    namedict = generate_mp_namedict(model, subset)
    write_dict_to_file(namedict, f'{model_name}_metabnames_mp.csv')

# Split reactions
irreversible = mpt.make_irreversible(model=model, subset=subset)
subset_irreversible = set(r.id for r in irreversible)

# Enumerate minimal sets of exchange reactions
# Enumeration is somewhat unstable? Why did I get 1306 the first time, but 1304 the second time? I thought the MPs were unique to one flux polyhedron?
mps, mcs, complete = mpt.find_mps(model,
                                  subset=subset_irreversible,
                                  method='iterative',
                                  graph=True,
                                  random=False,
                                  bounds=0,
                                  tol=1e-09,
                                  inf=1000,
                                  threads=0,
                                  max_mps=0,
                                  max_t=0,
                                  verbose=True,
                                  export=True)
