import cobra
import mptool as mpt
from functions import*
import numpy as np

model_name = 'e_coli_core'

# Load model
model = cobra.io.read_sbml_model(f'genome_scale_networks/models/{model_name}.xml')

# Define growth requirements for model for analysis of flux bounds
model.reactions.BIOMASS_Ecoli_core_w_GAM.lower_bound = 1e-4


# Modify boundaries
bound = 1000
for r in model.boundary:
    r.bounds = bound * np.sign(r.bounds)

# Define subset of reactions.
subset = [r.id for r in model.boundary]

# Split reactions
irreversible = mpt.make_irreversible(model=model, subset=subset)
subset_irreversible = set(r.id for r in irreversible)

# Enumerate minimal sets of exchange reactions
mps, mcs, complete = mpt.find_mps(model,
                                  subset=subset_irreversible,
                                  method='iterative',
                                  graph=True,
                                  random=False,
                                  bounds=0,
                                  tol=1e-09,
                                  inf=1000,
                                  threads=0,
                                  max_mps=0,
                                  max_t=0,
                                  verbose=True,
                                  export=True)