#!/bin/bash

#SBATCH --nodes=1                     # Number of nodes reserved
#SBATCH --ntasks=1                    # Number of threads reserved
#SBATCH --mem=8G                      # Amount of memory reserved
#SBATCH --partition=smallmem          # smallmem < 100GB
#SBATCH --job-name=randsamp           # Sensible name for the job
#SBATCH --output=randsamp_%j.log      # Logfile output here

# Run program in container
module purge

# Use my own code
singularity exec $HOME/containers/mptool/mp.sif python3 mp_hm.py
