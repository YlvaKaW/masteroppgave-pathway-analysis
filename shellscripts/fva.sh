#!/bin/bash

#SBATCH --nodes=1                      # Number of nodes reserved
#SBATCH --ntasks=1                     # Number of threads reserved
#SBATCH --mem=8G                       # Amount of memory reserved
#SBATCH --partition=smallmem           # smallmem < 100GB
#SBATCH --time=12:00:00                # Run for maximum 12 hours
#SBATCH --job-name=FVA                 # Sensible name for the job
#SBATCH --output=FVA_%j.log            # Logfile output here

module purge
singularity exec $HOME/containers/mptool/mp.sif python3 fva.py
