%% Vectors
% ECM vectors 
ep1 = [-1 -1 1]; ep2 = [-2 0 1]; ep3 = [-1 1 0]; 
% zero
zero = [0 0 0];

%% Surfaces
surf1 = [ep1;ep3;zero];

hold on; grid on; rotate3d on;
xlim([-10 0]);ylim([-10 10]);zlim([0 15]);
xlabel('A')
ylabel('B')
zlabel('BM')

% Plot one point
plot3(0, 0 , 0,'ob')

%% Draw lines
t = linspace(0, 100);
e1x = ep1(1)*t; e1y = ep1(2)*t; e1z = ep1(3)*t;
e2x = ep2(1)*t; e2y = ep2(2)*t; e2z = ep2(3)*t;
e3x = ep3(1)*t; e3y = ep3(2)*t; e3z = ep3(3)*t;

plot3(e1x,e1y,e1z,'r','linew',2);
plot3(e2x,e2y,e2z,'b','linew',2);
plot3(e3x,e3y,e3z,'r','linew',2);
plot3([-10 -10], [-10 10], [10 0], '--k', LineWidth=1.5);

%% Draw surfaces 
fill3(surf1(:,1)*100, surf1(:,2)*100, surf1(:,3)*100, [.5 0 .5], LineStyle='none')

alpha(0.20)

%% This is how to plot polyhedron 
% Each row is a surface, each column is a point in 3D.
%X = [0 -1   -1;0 -1    0;0  0    0;0   0   -1]*100;
%Y = [0  0    0;0  0   -1;0 -1   -1;0 -1     0]*100;
%Z = [0 0.1 100;0 100 100;0 100 0.1;0 0.1  0.1];

%surf(X, Y, Z)
%alpha(0.3)